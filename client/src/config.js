export const API_URL='http://localhost:3030'
export const CONFIG = {
  NUMBERS_MIN:-100,
  NUMBERS_MAX: 100,
  NUMBERS_SCALE_STEP: 10
}

export const lineChartDefaultOptions =
  {
    title: {
      text: 'Line chart'
    },
    xAxis: {
      type: 'datetime',
    },
    yAxis: {
      min: -100,
      max: 100,
    },
    series: [
      { data: [] }
    ],
  }

export const barChartDefaultOptions =
  {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Bar chart'
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '11px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Numbers received'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: 'Numbers: <b>{point.y:.0f}</b>'
    },
    series: [{}]
  }
