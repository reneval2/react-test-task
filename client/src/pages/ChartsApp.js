import React, { useEffect } from 'react'
import { newNumberReceived } from '../store/actions'
import { connect } from 'react-redux'
import io from 'socket.io-client'
import LineChart from '../components/LineChart'
import BarChart from '../components/BarChart'
import { API_URL } from '../config'
import { Grid, Container, Header, Divider } from 'semantic-ui-react'
import ThresholdInput from '../components/ThresholdInput'
import ValuesList from '../components/ValuesList'
import NotificationsFeed from '../components/NotificationsFeed'

function ChartsApp({ newNumberReceived }) {

  useEffect(() => {
    let socket = io(API_URL)
    socket.on('data', data => newNumberReceived(data))
    return function cleanup() {
      if (socket) {
        socket.close()
      }
    }
  })
  return (
    <Container>
      <NotificationsFeed />
      <Header as='h1'>Charts app</Header>
      <Grid columns={2} divided>
        <Grid.Row >
          <Grid.Column width={3} textAlign='left'>
            <ThresholdInput />
            <ValuesList />
          </Grid.Column>
          <Grid.Column width={13}>
            <LineChart />
            <Divider />
            <BarChart />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  )
}

const mapDispatchToProps = {
  newNumberReceived
}
export default connect(
  null,
  mapDispatchToProps
)(ChartsApp)