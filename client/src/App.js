import React from 'react';
import { Provider } from 'react-redux'
import 'semantic-ui-css/semantic.min.css'

import './App.css';
import store from './store/store'
import ChartsApp from './pages/ChartsApp';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <ChartsApp />
      </div>
    </Provider>
  );
}
export default(App);
