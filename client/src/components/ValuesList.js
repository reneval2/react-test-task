import React from 'react'
import { connect } from 'react-redux'
import { selectReversedValuesList } from '../store/selectors'

import { List } from 'semantic-ui-react'
import { setThreshold } from '../store/actions'

const generateKey = (pre) => {
  return `${ pre }_${ new Date().getTime() }`;
}

const ValuesList = ({ numbers }) => {
  return (
    <List bulleted>
      {numbers && numbers.map(value => (<List.Item key={generateKey(value) }>{value}</List.Item>))}
    </List>
  )
}

const mapStateToProps = (state) => ({
  numbers: selectReversedValuesList(state)
})

const mapDispathToProps = {
  setThreshold
}

export default connect(
  mapStateToProps,
  mapDispathToProps
)(ValuesList)