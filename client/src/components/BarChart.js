import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { selectAggregatedCategories } from '../store/selectors'
import HighchartsReact from 'highcharts-react-official'
import Highcharts from 'highcharts'

import { barChartDefaultOptions } from '../config'
import { Segment } from 'semantic-ui-react'

const BarChart = ({ categories }) => {
  const [chartOptions, setChartOptions] = useState(barChartDefaultOptions)
  useEffect(() => {
    setChartOptions((chartOptions) => ({
        ...chartOptions,
        series: [
          { data: categories }
        ],
      })
    )
  }, [categories])

  return (
    <Segment>
      <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions}
      />
    </Segment>
  )
}

const mapStateToProps = (state) => ({
  categories: selectAggregatedCategories(state)
})

export default connect(
  mapStateToProps
)(BarChart)