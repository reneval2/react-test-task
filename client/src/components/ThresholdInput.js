import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Input } from 'semantic-ui-react'

import { selectThreshold } from '../store/selectors'
import { setThreshold } from '../store/actions'
import { CONFIG } from '../config'

const getNewStateFromValue = (value) => {
  if (value === '') {
    return {
      isError: false,
      value: undefined
    }
  }
  const newValue = parseFloat(value)
  if (isNaN(newValue) || newValue < CONFIG.NUMBERS_MIN || newValue > CONFIG.NUMBERS_MAX) {
    return {
      isError: true,
      value: undefined
    }
  }
  return {
    isError: false,
    value: newValue
  }
}

const ThresholdInput = ({setThreshold}) => {
  const [state, setState] = useState({
    isError: false,
    value: undefined
  })

  const handleChange = (e, { name, value }) => {
    const newState = getNewStateFromValue(value)
    setState(newState)
    setThreshold(newState.value)
  }



  return (
    <Input placeholder='Threshold...'
           error={state.isError}
           name='threshold'
           type='number'
           onChange={handleChange} />
  )
}

const mapStateToProps = (state) => ({
  numbers: selectThreshold(state)
})

const mapDispathToProps = {
  setThreshold
}

export default connect(
  mapStateToProps,
  mapDispathToProps
)(ThresholdInput)