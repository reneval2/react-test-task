import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Message, Transition } from 'semantic-ui-react'

import { selectNotifications } from '../store/selectors'

const Wrapper = styled.div`
  position: absolute;
  top: 0px;
  right: 0px;
  display: flex;
  flex-flow: column nowrap;
  padding: 0.5em;
  z-index: 3000;
`

const CardWrapper = styled.div`
  margin: 0.5em;
`

const NotificationsFeed = ({ notifications }) => (
  <Transition.Group as={Wrapper} animation="fly left">
    {notifications.map(({id, text}) => (
      <CardWrapper key={id}>
        <Message negative>
          <Message.Content>
            <Message.Header></Message.Header>
            {text}
          </Message.Content>
        </Message>

      </CardWrapper>
    ))}
  </Transition.Group>
)

const mapStateToProps = state => ({
  notifications: selectNotifications(state),
})

export default connect(mapStateToProps)(NotificationsFeed)
