import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import HighchartsReact from 'highcharts-react-official'
import Highcharts from 'highcharts'
import { Segment } from 'semantic-ui-react'
import * as  R from 'ramda'

import { lineChartDefaultOptions } from '../config'
import { selectNumbersLists } from '../store/selectors'


const LineChart = ({ numbers }) => {
  const [chartOptions, setChartOptions] = useState(lineChartDefaultOptions)

  useEffect(() => {
    setChartOptions((chartOptions) => ({
        ...chartOptions,
        series: [
          { data: R.zipWith((x, y) => ({ x, y }), numbers.timestamps, numbers.values,) }
        ],
      })
    )
  }, [numbers])

  return (
    <Segment>
      <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions}
      />
    </Segment>
  )
}

const mapStateToProps = (state) => ({
  numbers: selectNumbersLists(state)
})

export default connect(
  mapStateToProps
)(LineChart)