import * as R from 'ramda'

export default (initialState, handlers) => (state = initialState, action) => {
  const handler = R.propOr(R.identity, action.type, handlers)
  return handler(state, action)
}
