import { actionTypes, hideNotification, showNotification } from './actions'
import { delay,filter, withLatestFrom, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'
import { selectThreshold } from './selectors'
import uuid from 'uuid/v4'

export const newNumberReceivedEpic = (action$, state$) => action$.pipe(
  filter(action => action.type === actionTypes.NUMBER_RECEIVED),
  withLatestFrom(state$),
  filter(([{ value }, state]) => selectThreshold(state) && value > selectThreshold(state)),
  switchMap(([{ value }, state]) => (of(showNotification({ id: uuid(), text: `Value received: ${JSON.stringify(value)}, Threshold:${selectThreshold(state)}` }))))
)


export const showNotificationEpic = (action$) => action$.pipe(
  filter(action => action.type === actionTypes.NOTIFICATION_SHOW),
  delay(4000),
  switchMap(({id}) => of(hideNotification({ id })))
)
