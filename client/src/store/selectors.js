import { createSelector } from 'reselect'
import * as  R from 'ramda'
import { CONFIG } from '../config'

const headLens = R.lensIndex(0)

const selectNumbers = R.path(['numbers'])

export const selectValuesList = createSelector(
  selectNumbers,
  R.prop('values')
)

export const selectReversedValuesList = createSelector(
  selectValuesList,
  R.reverse
)

export const selectNumbersLists = createSelector(
  selectNumbers,
  R.pick(['values', 'timestamps']),
)

export const selectThreshold = createSelector(
  selectNumbers,
  R.prop('threshold')
)

export const selectCategories = createSelector(
  selectNumbers,
  R.prop('category'),
)

export const selectNotifications = createSelector(
  selectNumbers,
  R.prop('notifications'),
)

export const selectAggregatedCategories = createSelector(
  selectCategories,
  R.pipe(
    R.toPairs,
    R.sortBy(
      R.compose(parseInt, R.prop(0))
    ),
    R.map(
      R.over(headLens, (a) => `[${a};${+a + CONFIG.NUMBERS_SCALE_STEP}]`)
    )
  )
)