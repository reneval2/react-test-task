export const actionTypes = {
  NUMBER_RECEIVED: 'NUMBER_RECEIVED',
  SET_THRESHOLD: 'SET_THRESHOLD',
  NOTIFICATION_SHOW: 'NOTIFICATION_SHOW',
  NOTIFICATION_HIDE: 'NOTIFICATION_HIDE',
}

export function newNumberReceived({ value, timestamp }) {
  return {
    type: actionTypes.NUMBER_RECEIVED,
    value,
    timestamp
  }
}

export function setThreshold(value) {
  return {
    type: actionTypes.SET_THRESHOLD,
    value,
  }
}

export function showNotification({id, text}) {
  return {
    type: actionTypes.NOTIFICATION_SHOW,
    id,
    text
  }
}

export function hideNotification({id}) {
  return {
    type: actionTypes.NOTIFICATION_HIDE,
    id,
  }
}
