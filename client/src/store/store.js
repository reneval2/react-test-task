import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import { combineEpics, createEpicMiddleware } from 'redux-observable'

import numbers from './reducers'
import { newNumberReceivedEpic,showNotificationEpic } from './epics'

const epicMiddleware = createEpicMiddleware()
const reducers = combineReducers({
  numbers
})
const middleware = [
  epicMiddleware
]

export const rootEpic = combineEpics(
  newNumberReceivedEpic,
  showNotificationEpic
)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  reducers,
  {},
  composeEnhancers(applyMiddleware(...middleware)),
)

epicMiddleware.run(rootEpic)
export default store
