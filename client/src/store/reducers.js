import { actionTypes } from './actions'
import createReducers from './createReducers'
import * as R from 'ramda'
import { CONFIG } from '../config'

const initialState = {
  values: [],
  timestamps: [],
  notifications: [],
  threshold: undefined,
  category: R.pipe(
    R.map(
      R.pipe(
        R.multiply(CONFIG.NUMBERS_SCALE_STEP),
        R.converge(R.pair, [R.identity, R.always(0)]),
      )
    ),
    R.fromPairs
  )(R.range(CONFIG.NUMBERS_MIN / CONFIG.NUMBERS_SCALE_STEP, CONFIG.NUMBERS_MAX / CONFIG.NUMBERS_SCALE_STEP))
}

const handlers = {
  [actionTypes.NUMBER_RECEIVED]: (state, { value, timestamp }) => {
    const categoryId = Math.floor(value / CONFIG.NUMBERS_SCALE_STEP) * CONFIG.NUMBERS_SCALE_STEP
    const categoryLens = R.lensProp(String(categoryId))
    return R.evolve(
      {
        values: R.append(value),
        timestamps: R.append(timestamp),
        category: R.over(categoryLens, R.inc)
      },
      state,
    )
  },
  [actionTypes.SET_THRESHOLD]: (state, { value }) => {
    return R.evolve(
      {
        threshold: R.always(value),
      },
      state,
    )
  },
  [actionTypes.NOTIFICATION_SHOW]: (state, { id, text }) => {
    return R.evolve(
      {
        notifications: R.append({ id, text }),
      },
      state,
    )
  },
  [actionTypes.NOTIFICATION_HIDE]: (state, { id }) => {
    return R.evolve(
      {
        notifications: R.reject(R.propEq('id', id)),
      },
      state,
    )
  }
}

export default createReducers(initialState, handlers)